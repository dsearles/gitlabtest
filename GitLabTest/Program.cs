﻿using Dependencies;
using System;

namespace GitLabTest
{
    class Program
    {
        static void Main(string[] args)
        {
            SqlDatabaseConnection conn = new SqlDatabaseConnection();
            Console.WriteLine($"My password is {conn.password}");
        }
    }
}
